# Instruções para utilizar o projeto.


** 1. Clonar o repositório**

1.1 git clone https://renatomc@bitbucket.org/renatomc/desafiopicpay.git

---

## Arquivo docker-compose.yml

Para executar o projeto, instale o Docker e o Docker-Compose.

1. Acessar via terminal a pasta em que está o arquivo docker-compose.yml.
2. Digitar `docker-compose up --build -d`.
3. Aguardar a montagem dos containers.

---

## Executando os comandos do Container

1. Após a subida dos containers, acessar o container **users-api-php**
2. Digitar no terminal **docker exec -it users-api-php /bin/sh**
3. Dentro do terminal digitar **composer install** para instalar e atualizar as dependências.
4. Dentro do terminal digitar **php artisan migrate** para gerar as tabelas do BD.

---
## Para testar a API via Swagger

1. Para testar a api basta acessar o endereço http://localhost:8000/api/documentation

---