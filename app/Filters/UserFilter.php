<?php

namespace App\Filters;

use App\Models\User;
use Illuminate\Http\Request;

class UserFilter
{
    public function index(Request $request, User $model)
    {
        $requestQuery = $request->query('q', '');

        if ($requestQuery) {
            $model = $model
                ->where('full_name', 'like', '%' . $requestQuery . '%')
                ->orWhereHas('consumer', function ($q) use ($requestQuery) {
                    $q->where('username', 'like', '%' . $requestQuery . '%');
                })
                ->orWhereHas('seller', function ($q) use ($requestQuery) {
                    $q->where('username', 'like', '%' . $requestQuery . '%');
                });
        }

        return $model;
    }
}
