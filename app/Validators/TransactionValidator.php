<?php

namespace App\Validators;

use App\Exceptions\EqualUsersInTransactionException;
use Illuminate\Support\Facades\Validator;

class TransactionValidator
{
    public function store(array $request)
    {
        if (
            isset($request['payee_id']) && isset($request['payer_id'])
            && ($request['payee_id'] == $request['payer_id'])
        ) {
            throw new EqualUsersInTransactionException();
        }

        return Validator::make(
            $request,
            [
                'payee_id' => [
                    'required',
                    'exists:users,id'
                ],
                'payer_id' => [
                    'required',
                    'exists:users,id'
                ],
                'value' => [
                    'required',
                    'numeric'
                ]
            ]
        );
    }
}
