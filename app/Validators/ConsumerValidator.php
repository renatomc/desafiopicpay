<?php

namespace App\Validators;

use Illuminate\Support\Facades\Validator;

class ConsumerValidator
{
    public function store(array $request)
    {
        return Validator::make(
            $request,
            [
                'username' => [
                    'required',
                    'max:30',
                    'unique:consumers',
                    'unique:sellers',
                ],
                'user_id' => [
                    'required',
                    'min:1',
                    'unique:consumers',
                    'exists:users,id'
                ]
            ],
            [
                'user_id.unique' => 'O usuário já possui uma conta do tipo consumer cadastrada!',
                'user_id.exists' => 'O usuário informado não existe!',
                'username.unique' => 'O nome de usuário informado já está em uso!'
            ]
        );
    }
}
