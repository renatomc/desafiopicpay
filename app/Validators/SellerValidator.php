<?php

namespace App\Validators;

use App\Rules\CNPJRule;
use Illuminate\Support\Facades\Validator;

class SellerValidator
{
    public function store(array $request)
    {
        return Validator::make(
            $request,
            [
                'cnpj' => [
                    'required',
                    'max:14',
                    'min:14',
                    'unique:sellers',
                    new CNPJRule()
                ],
                'fantasy_name' => [
                    'required',
                    'max:150'
                ],
                'social_name' => [
                    'required',
                    'max:255'
                ],
                'username' => [
                    'required',
                    'max:30',
                    'unique:consumers',
                    'unique:sellers',
                ],
                'user_id' => [
                    'required',
                    'min:1',
                    'unique:sellers',
                    'exists:users,id'
                ]
            ],
            [
                'user_id.unique' => 'O usuário já possui uma conta do tipo seller cadastrada!',
                'user_id.exists' => 'O usuário informado não existe!',
                'username.unique' => 'O nome de usuário informado já está em uso!'
            ]
        );
    }
}
