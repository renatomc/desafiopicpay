<?php

namespace App\Validators;

use App\Rules\CPFRule;
use Illuminate\Support\Facades\Validator;

class UserValidator
{
    public function store(array $request)
    {
        return Validator::make(
            $request,
            [
                'cpf' => [
                    'required',
                    'numeric',
                    'digits:11',
                    'unique:users',
                    new CPFRule()
                ],
                'email' => [
                    'required',
                    'email',
                    'max:80',
                    'unique:users'
                ],
                'full_name' => [
                    'required',
                    'max:255'
                ],
                'password' => [
                    'required',
                    'max:255'
                ],
                'phone_number' => [
                    'required',
                    'numeric',
                    'digits_between:10,11',
                    'unique:users'
                ]
            ]
        );
    }
}
