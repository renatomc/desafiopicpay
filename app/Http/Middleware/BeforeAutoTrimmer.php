<?php

namespace App\Http\Middleware;

use Closure;

class BeforeAutoTrimmer
{
	public function handle($request, Closure $next)
	{
		$request->merge(array_map('trim', $request->all()));
		$request->merge(array_map(function ($v) {
			if ($v == '' || $v == '/api/users') {
				$v = null;
			};
			return $v;
		}, $request->all()));
		return $next($request);
	}
}
