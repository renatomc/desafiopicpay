<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ConsumerService;
use Illuminate\Http\Request;

class ConsumerController extends Controller
{
    public function store(Request $request, ConsumerService $service)
    {
        return response()->json(
            $service->store($request),
            201
        );
    }
}
