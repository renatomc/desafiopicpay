<?php

namespace App\Http\Controllers;

use App\Services\TransactionService;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function store(Request $request, TransactionService $service)
    {
        return response()->json(
            $service->store($request),
            201
        );
    }

    public function show(TransactionService $service, $id)
    {
        return response()->json(
            $service->show($id),
            200
        );
    }
}
