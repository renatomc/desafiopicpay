<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request, UserService $service)
    {
        return response()->json(
            $service->index($request),
            200
        );
    }

    public function show(UserService $service, $id)
    {
        return response()->json(
            $service->show($id),
            200
        );
    }

    public function store(Request $request, UserService $service)
    {
        return response()->json(
            $service->store($request),
            201
        );
    }
}
