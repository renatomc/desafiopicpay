<?php

namespace App\Http\Controllers;

use App\Services\SellerService;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    public function store(Request $request, SellerService $service)
    {
        return response()->json(
            $service->store($request),
            201
        );
    }
}
