<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserDetailsResource extends Resource
{
    public function toArray($request)
    {
        $consumer = self::getConsumer();
        $seller = self::getSeller();

        if (!is_null($consumer->resource)) {
            $userDetail['accounts']['consumer'] = $consumer;
        }

        if (!is_null($seller->resource)) {
            $userDetail['accounts']['seller'] = $seller;
        }

        $userDetail['user'] = [
            'cpf' => $this->cpf,
            'email' => $this->email,
            'full_name' => $this->full_name,
            'id' => $this->id,
            'phone_number' => $this->phone_number
        ];

        return $userDetail;
    }

    public function getConsumer()
    {
        return new ConsumerResource($this->consumer);
    }

    public function getSeller()
    {
        return new SellerResource($this->seller);
    }
}
