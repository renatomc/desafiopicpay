<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    public function toArray($request)
    {
        return [
            'cpf' => $this->cpf,
            'email' => $this->email,
            'full_name' => $this->full_name,
            'id' => $this->id,
            'phone_number' => $this->phone_number
        ];
    }
}
