<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SellerResource extends Resource
{
    public function toArray($request)
    {
        return [
            'cnpj' => $this->cnpj,
            'fantasy_name' => $this->fantasy_name,
            'id' => $this->id,
            'social_name' => $this->social_name,
            'user_id' => $this->user_id,
            'username' => $this->username
        ];
    }
}
