<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ConsumerResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'username' => $this->username
        ];
    }
}
