<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TransactionResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'payee_id' => $this->payee_id,
            'payer_id' => $this->payer_id,
            'transaction_date' => $this->transaction_date,
            'value' => $this->value
        ];
    }
}
