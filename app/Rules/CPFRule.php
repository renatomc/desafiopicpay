<?php

namespace App\Rules;

use App\Helpers\ValidatorHelper;
use Illuminate\Contracts\Validation\Rule;

class CPFRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
       return ValidatorHelper::cpfValidator($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'CPF Inválido.';
    }
}
