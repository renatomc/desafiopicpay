<?php

namespace App\Rules;

use App\Helpers\ValidatorHelper;
use Illuminate\Contracts\Validation\Rule;

class CNPJRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
       return ValidatorHelper::cnpjValidator($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'CNPJ Inválido.';
    }
}
