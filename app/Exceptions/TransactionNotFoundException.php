<?php

namespace App\Exceptions;

class TransactionNotFoundException extends CustomException
{
    public function __construct($msg = 'Transação não encontrada!', $code = 404)
    {
        parent::__construct($msg, $code);
    }
}
