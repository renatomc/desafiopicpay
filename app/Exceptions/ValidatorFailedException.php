<?php

namespace App\Exceptions;

class ValidatorFailedException extends CustomException
{
    public function __construct($msg = 'Um ou mais campos obrigatórios não foram preenchidos!', $code = 422)
    {
        parent::__construct($msg, $code);
    }
}
