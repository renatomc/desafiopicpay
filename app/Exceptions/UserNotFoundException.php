<?php

namespace App\Exceptions;

class UserNotFoundException extends CustomException
{
    public function __construct($msg = 'Nenhum usuário encontrado!', $code = 404)
    {
        parent::__construct($msg, $code);
    }
}
