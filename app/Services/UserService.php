<?php

namespace App\Services;

use App\Exceptions\UserNotFoundException;
use App\Exceptions\ValidatorFailedException;
use App\Helpers\ValidatorHelper;
use App\Http\Resources\UserDetailsResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserResourceCollection;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        if ($request->query('q')) {
            $user = new UserResourceCollection($this->repository->findByRequest($request));
        } else {
            $user = new UserResourceCollection($this->repository->findAll());
        }

        if ($user->count() == 0) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    public function show($id)
    {
        $user = new UserDetailsResource($this->repository->find($id));

        if (is_null($user->resource)) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    public function store(Request $request)
    {
        $requestPost = $request->post();

        $validator = (new UserValidator)->store($requestPost);

        if ($validator->fails()) {
            throw new ValidatorFailedException(
                ValidatorHelper::arrayToStringMessage(
                    $validator
                        ->errors()
                        ->messages()
                )
            );
        }

        $requestPost['password'] = Hash::make($requestPost['password']);

        return new UserResource($this->repository->store($requestPost));
    }
}
