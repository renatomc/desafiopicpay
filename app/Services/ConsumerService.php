<?php

namespace App\Services;

use App\Exceptions\ValidatorFailedException;
use App\Helpers\ValidatorHelper;
use App\Http\Resources\ConsumerResource;
use App\Repositories\ConsumerRepository;
use App\Validators\ConsumerValidator;
use Illuminate\Http\Request;

class ConsumerService
{
    private $repository;

    public function __construct(ConsumerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store(Request $request)
    {
        $requestPost = $request->post();

        $validator = (new ConsumerValidator)->store($requestPost);

        if ($validator->fails()) {
            throw new ValidatorFailedException(
                ValidatorHelper::arrayToStringMessage(
                    $validator
                        ->errors()
                        ->messages()
                )
            );
        }

        return new ConsumerResource($this->repository->store($requestPost));
    }
}
