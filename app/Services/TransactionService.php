<?php

namespace App\Services;

use App\Exceptions\TransactionFailedException;
use App\Exceptions\TransactionNotFoundException;
use App\Exceptions\ValidatorFailedException;
use App\ExternalServices\TransactionExternalService;
use App\Helpers\ValidatorHelper;
use App\Http\Resources\TransactionResource;
use App\Repositories\TransactionRepository;
use App\Validators\TransactionValidator;
use Illuminate\Http\Request;

class TransactionService
{
    private $repository;

    public function __construct(TransactionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function show($id)
    {
        $transaction = new TransactionResource($this->repository->find($id));

        if (is_null($transaction->resource)) {
            throw new TransactionNotFoundException();
        }

        return $transaction;
    }

    public function store(Request $request)
    {
        $requestPost = $request->post();

        $validator = (new TransactionValidator)->store($requestPost);

        if ($validator->fails()) {
            throw new ValidatorFailedException(
                ValidatorHelper::arrayToStringMessage(
                    $validator
                        ->errors()
                        ->messages()
                )
            );
        }

        $returnTransaction = (new TransactionExternalService)->post($requestPost);

        if ($returnTransaction->getStatusCode() != 200 && $returnTransaction->getStatusCode() != 201) {
            $dadosRetorno = json_decode($returnTransaction->getBody()->getContents());
            throw new TransactionFailedException($dadosRetorno->message, $dadosRetorno->code);
        }

        $requestPost['transaction_date'] = date("Y-m-d H:i:s");

        return new TransactionResource($this->repository->store($requestPost));
    }
}
