<?php

namespace App\Services;

use App\Exceptions\ValidatorFailedException;
use App\Helpers\ValidatorHelper;
use App\Http\Resources\SellerResource;
use App\Repositories\SellerRepository;
use App\Validators\SellerValidator;
use Illuminate\Http\Request;

class SellerService
{
    private $repository;

    public function __construct(SellerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store(Request $request)
    {
        $requestPost = $request->post();

        $validator = (new SellerValidator)->store($requestPost);

        if ($validator->fails()) {
            throw new ValidatorFailedException(
                ValidatorHelper::arrayToStringMessage(
                    $validator
                        ->errors()
                        ->messages()
                )
            );
        }

        return new SellerResource($this->repository->store($requestPost));
    }
}
