<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model 
{
    protected $fillable = [
        'cpf',
        'email',
        'password',
        'full_name',
        'phone_number'
    ];

    public function consumer()
    {
        return $this->hasOne(Consumer::class);
    }

    public function seller()
    {
        return $this->hasOne(Seller::class);
    }
}
