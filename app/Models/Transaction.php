<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'payee_id',
        'payer_id',
        'value',
        'transaction_date'
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];
}
