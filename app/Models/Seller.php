<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected  $table = 'sellers';

    protected  $fillable = [
        'user_id',
        'cnpj',
        'fantasy_name',
        'social_name',
        'username'
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}