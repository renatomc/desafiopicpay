<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{
    protected  $table = 'consumers';

    protected  $fillable = [
        'user_id',
        'username'
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}