<?php

namespace App\ExternalServices;

use GuzzleHttp\Client;

class TransactionExternalService
{
    private $client;
    private $url;

    public function __construct()
    {
        $this->client = new Client();
        $this->url = 'http://www.transactions.tecnologia.ws/api/transactions';
    }

    public function post($requestPost)
    {
        $myBody = [
            'json' => $requestPost,
            'http_errors' => false
        ];

        return $this->client->post($this->url, $myBody);
    }
}
