<?php

namespace App\Repositories;

use App\Models\Consumer;

class ConsumerRepository
{
    private $model;

    public function __construct(Consumer $model)
    {
        $this->model = $model;
    }

    public function store($request)
    {
        return $this->model->create($request);
    }
}
