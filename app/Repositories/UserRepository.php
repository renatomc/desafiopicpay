<?php

namespace App\Repositories;

use App\Filters\UserFilter;
use App\Models\User;
use Illuminate\Http\Request;

class UserRepository
{
    private $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function findAll()
    {
        return $this->model->all();
    }

    public function findByRequest(Request $request)
    {
        $this->model = (new UserFilter())->index($request, $this->model);
        return $this->model->orderBy('full_name')->get();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function store($request)
    {
        return $this->model->create($request);
    }
}
