<?php

namespace App\Repositories;

use App\Models\Seller;

class SellerRepository
{
    private $model;

    public function __construct(Seller $model)
    {
        $this->model = $model;
    }

    public function store($request)
    {
        return $this->model->create($request);
    }
}