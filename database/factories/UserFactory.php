<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'cpf' => $faker->numberBetween(10000000000,99999999999),
        'email' => $faker->unique()->safeEmail,
        'full_name' => $faker->firstNameMale . ' ' . $faker->lastName,
        'password' => Hash::make('123456'),
        'phone_number' => $faker->numberBetween(11111111111,99999999999),
    ];
});
