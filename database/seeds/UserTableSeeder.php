<?php

use App\Models\Consumer;
use App\Models\Seller;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'cpf' => '50406840024',
                'email' => 'payee@picpay.com.br',
                'full_name' => 'PicPay Payee',
                'password' => Hash::make('123456'),
                'phone_number' => '11912345678'
            ],
            [
                'cpf' => '52019359073',
                'email' => 'payer@picpay.com.br',
                'full_name' => 'PicPay Payer',
                'password' => Hash::make('123456'),
                'phone_number' => '11923456789'
            ]
        ];

        $sellers = [
            '50406840024' => [
                'cnpj' => '34348224000100',
                'fantasy_name' => 'Payee Fantasy Name',
                'social_name' => 'Payee Social Name',
                'username' => 'payee.seller'
            ],
            '52019359073' => [
                'cnpj' => '64649416000118',
                'fantasy_name' => 'Payer Fantasy Name',
                'social_name' => 'Payer Social Name',
                'username' => 'payer.seller'
            ]
        ];

        $consumers = [
            '50406840024' => [
                'username' => 'payee.consumer'
            ],
            '52019359073' => [
                'username' => 'payer.consumer'
            ]
        ];

        foreach ($users as $user) {
            $u = User::create($user);
            $u->seller()->save(Seller::make($sellers[$user['cpf']]));
            $u->consumer()->save(Consumer::make($consumers[$user['cpf']]));
        }

        // factory(User::class, 2)->create();
    }
}
