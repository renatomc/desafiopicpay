<?php

use Faker\Factory as Faker;

class UserTest extends  TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStore201()
    {
        $faker = Faker::create();

        $user = [
           'cpf' => $faker->numberBetween(11111111111,99999999999),
           'email' => $faker->email,
           'full_name' => $faker->firstName.' '.$faker->lastName,
           'password' => $faker->numberBetween(111111, 999999),
           'phone_number' =>  $faker->numberBetween(11111111111, 99999999999),
       ];

        $this->post('/api/users', $user);
        $this->assertResponseStatus(201);

        $resposta = (array) json_decode($this->response->content());

        $this->assertArrayHasKey('cpf', $resposta);
        $this->assertArrayHasKey('email', $resposta);
        $this->assertArrayHasKey('full_name', $resposta);
        $this->assertArrayHasKey('password', $resposta);
        $this->assertArrayHasKey('phone_number', $resposta);
        $this->assertArrayHasKey('id', $resposta);
    }

    public function testStore422()
    {
        $faker = Faker::create();

        $user = [
            'cpf' => $faker->numberBetween(11111111111,99999999999),
            'email' => '',
            'full_name' => $faker->firstName.' '.$faker->lastName,
            'password' => $faker->numberBetween(111111, 999999),
            'phone_number' =>  $faker->numberBetween(11111111111, 99999999999),
        ];

        $this->post('/api/users', $user);

        $this->assertResponseStatus(422);
    }
}
