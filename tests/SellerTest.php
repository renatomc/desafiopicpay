<?php

use Faker\Factory as Faker;

class SellerTest extends  TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStore201()
    {
        $faker = Faker::create();

        $seller = [
            'cnpj' => $faker->numberBetween(11111111111111,99999999999999),
            'fantasy_name' => $faker->firstName,
            'social_name' => $faker->firstName.' '.$faker->lastName,
            'user_id' => 1,
            'username' =>  $faker->userName
        ];

        $this->post('/api/users/sellers', $seller);
        $this->assertResponseStatus(201);

        $resposta = (array) json_decode($this->response->content());

        $this->assertArrayHasKey('cnpj', $resposta);
        $this->assertArrayHasKey('fantasy_name', $resposta);
        $this->assertArrayHasKey('id', $resposta);
        $this->assertArrayHasKey('social_name', $resposta);
        $this->assertArrayHasKey('user_id', $resposta);
        $this->assertArrayHasKey('username', $resposta);
    }

    public function testStore404()
    {
        $faker = Faker::create();

        $seller = [
            'cnpj' => $faker->numberBetween(11111111111111,99999999999999),
            'fantasy_name' => $faker->firstName,
            'social_name' => $faker->firstName.' '.$faker->lastName,
            'user_id' => 90000000,
            'username' =>  $faker->userName
        ];

        $this->post('/api/users/sellers', $seller);
        $this->assertResponseStatus(404);
    }

    public function testStore422()
    {
        $faker = Faker::create();

        $seller = [
            'cnpj' => '',
            'fantasy_name' => $faker->firstName,
            'social_name' => $faker->firstName.' '.$faker->lastName,
            'user_id' => 90000000,
            'username' =>  $faker->userName
        ];

        $this->post('/api/users/sellers', $seller);
        $this->assertResponseStatus(422);
    }
}
