<?php

use Faker\Factory as Faker;

class TransactionTest extends  TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStore201()
    {
        $transaction = [
           'payee_id' => 1,
           'payer_id' => 2,
           'value' => 99
       ];

        $this->post('/api/transactions', $transaction);
        $this->assertResponseStatus(201);

        $resposta = (array) json_decode($this->response->content());

        $this->assertArrayHasKey('id', $resposta);
        $this->assertArrayHasKey('payee_id', $resposta);
        $this->assertArrayHasKey('payer_id', $resposta);
        $this->assertArrayHasKey('transaction_date', $resposta);
        $this->assertArrayHasKey('value', $resposta);
    }

    public function testStore401()
    {
        $transaction = [
            'payee_id' => 1,
            'payer_id' => 2,
            'value' => 100
        ];

        $this->post('/api/transactions', $transaction);
        $this->assertResponseStatus(401);
    }

    public function testStore422()
    {
        $transaction = [
            'payee_id' => '',
            'payer_id' => 2,
            'value' => 100
        ];

        $this->post('/api/transactions', $transaction);
        $this->assertResponseStatus(422);
    }
}
