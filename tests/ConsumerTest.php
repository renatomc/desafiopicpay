<?php

use Faker\Factory as Faker;

class ConsumerTest extends  TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStore201()
    {
        $faker = Faker::create();

        $consumer = [
            'user_id' => 1,
            'username' => $faker->firstName
       ];

        $this->post('/api/users/consumers', $consumer);
        $this->assertResponseStatus(201);

        $resposta = (array) json_decode($this->response->content());

        $this->assertArrayHasKey('id', $resposta);
        $this->assertArrayHasKey('user_id', $resposta);
        $this->assertArrayHasKey('username', $resposta);
    }

    public function testStore404()
    {
        $faker = Faker::create();

        $consumer = [
            'user_id' => 90000000,
            'username' => $faker->userName
        ];

        $this->post('/api/users/consumers', $consumer);
        $this->assertResponseStatus(404);
    }

    public function testStore422()
    {
        $consumer = [
            'user_id' => 1,
            'username' => ''
        ];

        $this->post('/api/users/consumers', $consumer);
        $this->assertResponseStatus(422);
    }
}
